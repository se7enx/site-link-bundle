<?php

namespace ContextualCode\SiteLinkBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ContextualCodeSiteLinkBundle extends Bundle
{
    protected $name = 'ContextualCodeSiteLinkBundle';
}
