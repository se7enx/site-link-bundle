<?php

namespace ContextualCode\SiteLinkBundle\Routing\Generator;

use ContextualCode\SiteLinkBundle\Services\SiteAccessHostMatcher;
use Exception;
use eZ\Publish\API\Repository\Repository;
use eZ\Publish\API\Repository\Values\Content\Location;
use eZ\Publish\Core\MVC\ConfigResolverInterface;
use eZ\Publish\Core\MVC\Symfony\Routing\Generator\UrlAliasGenerator;
use function in_array;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;

/**
 * Overrides eZ's UrlAliasGenerator to fix cross siteaccess urls
 * and linking to files, images, and internal/external links.
 */
class SiteUrlAliasGenerator extends UrlAliasGenerator
{
    /**
     * Map of identifiers of file content types and attributes.
     *
     * @var array
     */
    public static $fileConfig = [];

    /**
     * @var int[]
     */
    public static $fileContentTypeIds = [];

    /**
     * Map of identifiers of image content types and attributes.
     *
     * @var array
     */
    public static $imageConfig = [];

    /**
     * Map of identifiers of image content types and attributes.
     *
     * @var array
     */
    public static $ancestorConfig = [];

    /**
     * @var int[]
     */
    public static $imageContentTypeIds = [];

    /**
     * @var int[]
     */
    public static $ancestorContentTypeIds = [];

    /**
     * List of content types that should not be linked to directly
     * but rather contain internal/external link attributes.
     *
     * @var array
     */
    public static $linkConfig = [];

    /**
     * @var int[]
     */
    public static $linkContentTypeIds = [];

    /**
     * @var \eZ\Publish\Core\Repository\Repository
     */
    protected $repository;

    /**
     * The default router (that works with declared routes).
     *
     * @var RouterInterface
     */
    protected $defaultRouter;

    /**
     * @var eZ\Publish\Core\MVC\Symfony\SiteAccess
     */
    protected $siteaccess;

    /**
     * @var ConfigResolverInterface
     */
    protected $configResolver;

    /**
     * @var Project\SiteBundle\Services\SiteAccessHostMatcher
     */
    protected $siteAccessHostMatcher;

    protected $imageVariationHandler;

    public function __construct(
        Repository $repository,
        RouterInterface $defaultRouter,
        ConfigResolverInterface $configResolver,
        SiteAccessHostMatcher $siteAccessHostMatcher,
        $imageVariationHandler,
        $config
    ) {
        $this->repository = $repository;
        $this->defaultRouter = $defaultRouter;
        $this->configResolver = $configResolver;
        parent::__construct($repository, $defaultRouter, $configResolver);
        $this->siteAccessHostMatcher = $siteAccessHostMatcher;
        $this->imageVariationHandler = $imageVariationHandler;

        $this->hostMatchMethod = $config['siteaccess_host_match_method'];
        foreach ($config['direct_file_link'] as $c) {
            self::$fileConfig[$c['class_identifier']] = [
                'class_identifier' => $c['class_identifier'],
                'attribute_identifier' => $c['attribute_identifier'],
            ];
        }
        foreach ($config['direct_image_link'] as $c) {
            self::$imageConfig[$c['class_identifier']] = [
                'class_identifier' => $c['class_identifier'],
                'attribute_identifier' => $c['attribute_identifier'],
            ];
        }
        foreach ($config['ancestor_link'] as $c) {
            self::$ancestorConfig[$c['class_identifier']] = [
                'class_identifier' => $c['class_identifier'],
                'ancestor_level' => $c['ancestor_level'],
            ];
        }
        foreach ($config['internal_external'] as $c) {
            $new = [
                'class_identifier' => $c['class_identifier'],
            ];
            if (isset($c['internal_attribute_identifier'])) {
                $new['internal'] = $c['internal_attribute_identifier'];
            }
            if (isset($c['external_attribute_identifier'])) {
                $new['external'] = $c['external_attribute_identifier'];
            }
            self::$linkConfig[$c['class_identifier']] = $new;
        }
    }

    public function generate($urlResource, array $parameters, $absolute = false)
    {
        // if we're in a preview from admin siteaccess, set that so we can link absolutely
        $request = $this->siteAccessHostMatcher->getRequest();
        $isPreview = false;
        if ($request instanceof Request) {
            if (preg_match('/\/content\/versionview\/(.*)\/site_access\/(.*)\/?/', $request->getUri(), $matches) === 1) {
                $isPreview = true;
            }
        }

        $crossSA = !isset($parameters['site-link-cross-siteaccess']) || $parameters['site-link-cross-siteaccess'];
        unset($parameters['site-link-cross-siteaccess']);

        $url = parent::generate($urlResource, $parameters, $absolute);
        if (!($urlResource instanceof Location)) {
            return $url;
        }

        // image content types
        if (!self::$imageContentTypeIds) {
            self::$imageContentTypeIds = $this->loadContentTypeIds(self::$imageConfig);
        }
        $contentTypeId = $urlResource->contentInfo->contentTypeId;
        if (in_array($contentTypeId, self::$imageContentTypeIds, true)) {
            return $url;
        }

        $siteaccess = $this->siteAccessHostMatcher->getLocationSiteAccess($urlResource);

        // fix host name if url points to a different site access. Check that siteaccess is set so that module-based urls are not all made absolute
        if ($siteaccess != '' && $crossSA && ($siteaccess != $this->siteAccess->name || $isPreview)) {
            $parameters['siteaccess'] = $siteaccess;
            // re-generate with new siteaccess
            $url = parent::generate($urlResource, $parameters, $absolute);

            // remove siteaccess matched part of url if present
            if (method_exists($this->siteAccess->matcher, 'analyseURI')) {
                $url = $this->siteAccess->matcher->analyseURI($url);
            }
            // strip host
            $queryStr = parse_url($url, PHP_URL_QUERY);
            $url = parse_url($url, PHP_URL_PATH) . ($queryStr ? "?{$queryStr}" : '');
            // attempt to find best host for url
            if ($this->hostMatchMethod == 'first') {
                $url = $this->siteAccessHostMatcher->matchFirstHost($siteaccess) . $url;
            } else {
                if ($this->hostMatchMethod == 'best') {
                    $url = $this->siteAccessHostMatcher->matchBestHost($siteaccess) . $url;
                }
            }
        }

        // link content types
        if (!self::$linkContentTypeIds) {
            self::$linkContentTypeIds = $this->loadContentTypeIds(self::$linkConfig);
        }
        $contentTypeId = $urlResource->contentInfo->contentTypeId;
        $contentTypeName = array_search($contentTypeId, self::$linkContentTypeIds);

        if ($contentTypeName !== false) {
            $contentService = $this->repository->getContentService();
            $content = $contentService->loadContentByContentInfo($urlResource->contentInfo);
            if (isset(self::$linkConfig['all'])) {
                $linkConfig = self::$linkConfig['all'];
            } else {
                $linkConfig = self::$linkConfig[$contentTypeName];
            }
            if (isset($linkConfig['external'])) {
                $externalLink = $content->getFieldValue($linkConfig['external']);
            }
            if (isset($linkConfig['internal'])) {
                $internalLink = $content->getFieldValue($linkConfig['internal']);
            }
            if (isset($externalLink) && $externalLink && isset($externalLink->link) && !empty($externalLink->link)) {
                return $externalLink->link;
            } else {
                if (isset($externalLink) && $externalLink && isset($externalLink->text) && !empty($externalLink->text)) {
                    return $externalLink->text;
                } else {
                    if (isset($internalLink) && $internalLink && isset($internalLink->destinationContentIds) && count($internalLink->destinationContentIds)) {
                        $destinationContent = $this->repository->sudo(function () use ($contentService, $internalLink) {
                            return $contentService->loadContent($internalLink->destinationContentIds[0]);
                        });
                        $location = $this->repository->sudo(function () use ($destinationContent) {
                            return $this->repository->getLocationService()->loadLocation($destinationContent->contentInfo->mainLocationId);
                        });
                        if ($location->id === $urlResource->id) {
                            return '';
                        }

                        return $this->generate($location, $parameters, $absolute);
                    }
                    else if (isset($internalLink) && $internalLink && isset($internalLink->destinationContentId) && $internalLink->destinationContentId) {
                        $destinationContent = $this->repository->sudo(function () use ($contentService, $internalLink) {
                            return $contentService->loadContent($internalLink->destinationContentId);
                        });
                        $location = $this->repository->sudo(function () use ($destinationContent) {
                            return $this->repository->getLocationService()->loadLocation($destinationContent->contentInfo->mainLocationId);
                        });
                        if ($location->id === $urlResource->id) {
                            return '';
                        }

                        return $this->generate($location, $parameters, $absolute);
                    }
                }
            }
        }

        return $url;
    }

    public function doGenerate($location, array $parameters)
    {
        if (!$location) {
            return '';
        }

        if (!($location instanceof Location)) {
            // generate url
            return parent::doGenerate($location, $parameters);
        }

        // set root location id based on siteaccess associated with location
        $rootLocationId = $this->siteAccessHostMatcher->getLocationRootLocationId($location);
        if ($rootLocationId) {
            $this->setRootLocationId($rootLocationId);
        }

        $contentTypeId = $location->contentInfo->contentTypeId;

        // media files
        if (!self::$fileContentTypeIds) {
            self::$fileContentTypeIds = $this->loadContentTypeIds(self::$fileConfig);
        }
        $contentTypeName = array_search($contentTypeId, self::$fileContentTypeIds);
        if ($contentTypeName !== false) {
            $content = $this->repository->sudo(function () use ($location) {
                return $this->repository->getContentService()->loadContentByContentInfo($location->contentInfo);
            });
            $fileField = self::$fileConfig[$contentTypeName]['attribute_identifier'];
            $routes = $this->defaultRouter->getRouteCollection();
            $route = $routes->get('ez_content_download');
            if (strpos($route->getPath(), 'fieldIdentifier') === false) {
                return sprintf(
                    '/content/download/%d/%d/%s',
                    $content->id,
                    $content->getField($fileField)->id,
                    $content->getFieldValue($fileField)->fileName
                );
            } else {
                return sprintf(
                    '/content/download/%d/%s/%s',
                    $content->id,
                    $fileField,
                    $content->getFieldValue($fileField)->fileName
                );
            }
        }

        // media images
        if (!self::$imageContentTypeIds) {
            self::$imageContentTypeIds = $this->loadContentTypeIds(self::$imageConfig);
        }
        $contentTypeName = array_search($contentTypeId, self::$imageContentTypeIds);
        if ($contentTypeName !== false) {
            $content = $this->repository->sudo(function () use ($location) {
                return $this->repository->getContentService()->loadContentByContentInfo($location->contentInfo);
            });
            $imageField = self::$imageConfig[$contentTypeName]['attribute_identifier'];
            $imageField = $content->getField($imageField);
            try {
                $variation = $this->imageVariationHandler->getVariation(
                    $imageField, $content->versionInfo, 'original'
                );
            } catch (Exception $e) {
                return '';
            }

            return $variation->uri;
        }

        // ancestor linking
        if (!self::$ancestorContentTypeIds) {
            self::$ancestorContentTypeIds = $this->loadContentTypeIds(self::$ancestorConfig);
        }
        $contentTypeName = array_search($contentTypeId, self::$ancestorContentTypeIds);
        if ($contentTypeName !== false) {
            $ancestor_level = self::$ancestorConfig[$contentTypeName]['ancestor_level'];
            $new_location = $location;
            for ($x = 0; $x < $ancestor_level; ++$x) {
                $new_location = $this->repository->getLocationService()->loadLocation($new_location->parentLocationId);
            }

            return self::doGenerate($new_location, $parameters);
        }

        // generate url
        return parent::doGenerate($location, $parameters);
    }

    protected function loadContentTypeIds($config)
    {
        if (!isset($this->contentTypeService)) {
            $this->contentTypeService = $this->repository->getContentTypeService();
        }
        $ctIds = [];
        foreach ($config as $id => $c) {
            try {
                $ctIds[$id] = $this->contentTypeService->loadContentTypeByIdentifier($id)->id;
            } catch (Exception $e) {
                continue;
            }
        }

        return $ctIds;
    }
}
