# SiteLink Bundle
This is an eZ Platform / eZ Publish 5 Symfony bundle to fix cross-siteaccess links, linking media types, etc.

## Installation
Run `composer require`:

```bash
$ composer require contextualcode/site-link-bundle
```

Enable the bundle in `app/AppKernel.php` (`ezpublish/EzPublishKernel.php`) by adding this line in the `registerBundles` method:

```php
    public function registerBundles()
    {
        $bundles = array(
            ...
            new ContextualCode\SiteLinkBundle\ContextualCodeSiteLinkBundle(),
            ...
        );
``` 

## Usage

Usage is automatic once the bundle is enabled and configured.

You can use `site-link-cross-siteaccess` parameter to disable cross siteaccess linking.
For example:
```twig
{{ path( 'ez_urlalias', { 'locationId': 2, 'site-link-cross-siteaccess': false }) }}
```
```php
$router->generate('ez_urlalias', ['locationId' => 2, 'site-link-cross-siteaccess' => false]);
```

## Sample config entry

The bundle contains a few options that can be configured in `config.yml`. For example:

```
contextual_code_site_link:
    siteaccess_host_match_method: "first"

    internal_external:
        - class_identifier: link
          external_attribute_identifier: external_link
          internal_attribute_identifier: internal_link
        - class_identifier: banner
          external_attribute_identifier: external_link
          internal_attribute_identifier: internal_link

    direct_file_link:
        - class_identifier: file
          attribute_identifier: file

    direct_image_link:
        - class_identifier: image
          attribute_identifier: image
```

- **siteaccess\_host\_match\_method**: either "none", "first", or "best". This controls how cross-siteaccess linking will be handled. "first" will use the first domain available in your siteaccess config. "best" will use the domain from the siteaccess with the most matching parts as the current domain.

- **internal\_external**: expects an array, each with **class_identifier**, optional **external\_attribute**, and optional **internal\_attribute**. Will treat these as "link externally if possible, and if not, link to the internal content id".

- **direct\_file\_link**: expects an array, each with **class\_identifier** and **attribute\_identifier** to a File field type. Links to these objects will link directly to the /content/download file link.

- **direct\_image\_link**: Same as **direct\_file\_link**, but will link directly to the image.
